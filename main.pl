/*
 * main.pl
 * This file is part of Search-and-evaluate-sessions-in-Prolog
 *
 * Copyright (C) 2016 - x
 *
 * Search-and-evaluate-sessions-in-Prolog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Search-and-evaluate-sessions-in-Prolog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Search-and-evaluate-sessions-in-Prolog. If not, see <http://www.gnu.org/licenses/>.
 */
 
 
%This predicate used from user to make a query to the system.
%If user doesn't specify a file then the default is 'sessions.pl'
query(Question):-
	handle_queries('sessions.pl',Question).
query(Question,X):-
	handle_queries(X,Question).

%Predicate to manage flow of main actions for program
handle_queries(File,Question):-
	check_question_format(Question),       %Checks if the question is correct
	consult(File),                         %Reads the file with records
	findall([Titles,Topics], session(Titles,Topics), List), %Load all sessions to 'List'
	question_handle(List,Question,Scores),       %Main action to calculate the numbers
	qsort(Scores,Sorted),		        %Use qsort to sort all sessions by number
	print_sorted_list(Sorted),
%	write(Sorted),nl,                       %Print the results
	nl.

%Iterate all sessions
%Scores must be in format [Title,Score]
question_handle([],_,_).%---to orisma sto telos na elexw
question_handle([[Title,[TopicsH|TopicsT]]|Tail], QuestionL, [ScoreH|ScoreT]):-
	%session(X,Y),
	question_evaluate(Title,[TopicsH|TopicsT],QuestionL,ScoreH),
	question_handle(Tail,QuestionL,ScoreT).

%Iterate sorted list
print_sorted_list([]).
print_sorted_list([[Title,Score]|Tail]) :-
	write('Session: '), write(Title), nl,
	write('    Relevance = '), write(Score), nl,
	print_sorted_list(Tail).

%This calculates only a session
%Title is the title of a session
%Topics are all topics of a session
%QuestionL is the list with the words of user's query
%[Title,Score] returned as result
question_evaluate(Title, TopicsL,QuestionL,[Title,Score]):-
	title_calculate(Title,QuestionL, TitleSum1),    %Calculates only the title of a session
%	write('TitleSum: '), write(TitleSum1),nl,       %Calculate Title
	topic_iterator(TopicsL,QuestionL,TopicSum),     %Calculates only the topics of a session
%	write('TopicSum: '), write(TopicSum),nl,        %TopicSum must be a list with all values
	TitleSum is TitleSum1*2,			%Title must multiply by 2
%	write([TitleSum|TopicSum]), nl,
	max_list([TitleSum|TopicSum],Multiply),	        %Multiply is the max of the list to multiply
	sum_list([TitleSum|TopicSum],Add),              %Used to add all the points to session score
	Score is Add + 1000*Multiply.			%Score returned back to list with Title


%Calculate score of the session's title
%Iterate all query words
%TitleSum returned to question_evaluate(
title_calculate(_,[],0).
title_calculate(Title,[QuestionH|QuestionT], TitleSum):-
	title_calculate(Title, QuestionT, Temp),           %recursion to iterate all questions
	koula(A1,Weigth,QuestionH),
	(sub_string(case_insensitive,A1,Title) -> Weight2 is Weigth;Weight2 is 0),
	tokenize_atom(A1, L),                   %split all words
	length(L,Division),
	(Division = 1 -> ContainsNumber is 0;list_recursive(Title,L,ContainsNumber)),
	TitleSum is Temp + Weight2 + Weigth/Division *ContainsNumber.

koula(A1,A2,A1-A2).

koula(A1,1,A1).


list_recursive(_Title,[],0).
list_recursive(Title,[QuestionTokens|T],Number):-
	list_recursive(Title,T,TempNumber),
	(   sub_string(case_insensitive,QuestionTokens,Title)->Number is TempNumber+1; Number is  TempNumber).


%Predicate to iterate all topics.
%
topic_iterator([], _QuestionL, []).
topic_iterator([TopicH|TopicT], QuestionL, [FinalScoreH|FinalScoreT]) :-
	title_calculate(TopicH, QuestionL, FinalScoreH),
	topic_iterator(TopicT, QuestionL, FinalScoreT).

qsort([],[]).
qsort([[A|B]|L],Sorted) :-
	split([A|B],L,Small,Big),
	qsort(Small,SortedSmall),
	qsort(Big,SortedBig),
	append(SortedSmall,[[A|B]|SortedBig],Sorted).

split([A,B],[],[],[]).
split([A,B],[[C,D]|T],[[C,D]|Small],Big) :- B =< D,
	split([A,B],T,Small,Big).
split([A,B],[[C,D]|T],Small,[[C,D]|Big]) :- B > D,
	split([A,B],T,Small,Big).

% Checks if user query is in the right format
check_question_format(Question) :-
	is_list(Question),!.
check_question_format(_Question):-
	write('You should ask in format: "query([''word/s to check''])."!'), fail.
